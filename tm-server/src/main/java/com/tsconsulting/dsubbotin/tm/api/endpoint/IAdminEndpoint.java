package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void dataLoadJsonJaxb(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataSaveJsonJaxb(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataLoadXmlJaxb(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataSaveXmlJaxb(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataLoadJsonFasterXml(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataSaveJsonFasterXml(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataLoadXmlFasterXml(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void dataSaveXmlFasterXml(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void loadBackup(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void saveBackup(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void loadBinary(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void saveBinary(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void loadBase64(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

    @WebMethod
    void saveBase64(
            @Nullable @WebParam(name = "session") SessionDTO session
    ) throws Exception;

}
