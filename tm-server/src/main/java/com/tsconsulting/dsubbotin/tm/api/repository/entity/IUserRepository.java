package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public interface IUserRepository extends IRepository<User> {

    boolean existById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException;

    void clear(@NotNull String userId);

}
