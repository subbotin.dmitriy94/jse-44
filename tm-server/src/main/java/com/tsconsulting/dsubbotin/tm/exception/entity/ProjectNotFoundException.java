package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Project not found!");
    }

}