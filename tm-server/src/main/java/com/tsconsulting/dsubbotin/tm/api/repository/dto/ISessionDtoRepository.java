package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;

public interface ISessionDtoRepository extends IDtoRepository<SessionDTO> {

    void open(@NotNull SessionDTO session);

    void close(@NotNull SessionDTO session);

    boolean contains(@NotNull SessionDTO session);

    boolean existByUserId(@NotNull String id);

}
