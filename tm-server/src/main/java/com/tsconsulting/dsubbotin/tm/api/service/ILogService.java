package com.tsconsulting.dsubbotin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILogService {

    void info(@NotNull String message);

    void debug(@NotNull String message);

    void command(@NotNull String message);

    void error(@NotNull Exception e);

}