package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.api.service.dto.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    ISessionDtoService getSessionService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}
