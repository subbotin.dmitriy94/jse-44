package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.entity.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.ITaskService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IUserService;
import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.service.entity.ProjectService;
import com.tsconsulting.dsubbotin.tm.service.entity.ProjectTaskService;
import com.tsconsulting.dsubbotin.tm.service.entity.TaskService;
import com.tsconsulting.dsubbotin.tm.service.entity.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public final class TaskServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "taskNameTest";

    @NotNull
    private final String taskDescription = "taskDescriptionTest";

    @NotNull
    private final String userId;

    @Nullable
    private String projectId;

    public TaskServiceTest() throws AbstractException {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService();
        userService = new UserService(connectionService, logService);
        taskService = new TaskService(connectionService, logService);
        projectService = new ProjectService(connectionService, logService);
        projectTaskService = new ProjectTaskService(connectionService, logService);
        userId = userService.create("test", "test").getId();
        projectId = projectService.create(userId, "projectTest", "projectTest").getId();
        taskId = taskService.create(userId, taskName, taskDescription).getId();
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        @NotNull final Task newTask = taskService.create(userId, newTaskName, newTaskDescription);
        Assert.assertNotNull(taskService.findByName(userId, taskName));
        Assert.assertEquals(newTask.getName(), newTaskName);
        Assert.assertEquals(newTask.getDescription(), newTaskDescription);
        taskService.removeById(userId, newTask.getId());
    }

    @Test
    public void findByName() throws AbstractException {
        @NotNull final Task task = taskService.findByName(userId, taskName);
        Assert.assertEquals(task.getName(), taskName);
        Assert.assertEquals(task.getDescription(), taskDescription);
        Assert.assertEquals(task.getUser().getId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateById(userId, taskId, newTaskName, newTaskDescription);
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUser().getId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateByIndex(userId, 1, newTaskName, newTaskDescription);
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUser().getId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startById(userId, taskId);
        @NotNull final Task updTempTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(userId, 1);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByIndex(userId, 1);
        @NotNull final Task updTempTask = taskService.findByIndex(userId, 1);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByName(userId, taskName);
        @NotNull final Task updTempTask = taskService.findByName(userId, taskName);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
        taskService.finishById(userId, taskId);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
        taskService.finishByIndex(userId, 1);
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
        taskService.finishByName(userId, taskName);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.COMPLETED);
        taskService.updateStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByIndex(userId, 1, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByIndex(userId, 1, Status.COMPLETED);
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.COMPLETED);
        taskService.updateStatusByIndex(userId, 1, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.COMPLETED);
        taskService.updateStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertNull(taskService.findById(userId, taskId).getProject());
        Assert.assertNotNull(projectId);
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @NotNull final Task updTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(updTask.getProject());
        Assert.assertNotNull(updTask.getProject().getId());
        Assert.assertEquals(updTask.getProject().getId(), projectId);
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(projectId);
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertNull(taskService.findById(userId, taskId).getProject());
    }

    @Test
    public void findAllTasksByProjectId() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(projectId);
        @NotNull final List<Task> tasks = projectTaskService.findAllTasksByProjectId(userId, projectId);
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProject());
        Assert.assertNotNull(projectId);
        projectTaskService.removeProjectById(userId, projectId);
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProject());
    }

    @Test
    public void removeProjectByIndex() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProject());
        projectTaskService.removeProjectByIndex(userId, 1);
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProject());
    }

    @Test
    public void removeProjectByName() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProject());
        projectTaskService.removeProjectByName(userId, "projectTest");
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProject());
    }

    @After
    public void finalizeTest() throws AbstractException {
        taskService.removeById(userId, taskId);
        if (projectId != null) projectService.clear(userId);
        userService.removeById(userId);
    }

}
