package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable final SessionDTO session);

}
